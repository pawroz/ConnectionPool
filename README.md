# Connection pool app   
Connection pool is a program to manage connections of database and execute any kind of database queries.
The app use many threads to work faster and optimized.


# Usage
To run a program you have to fill database credentials in **database.ini** file.

Sample completed database.ini file is in repository.

A mandatory step is to launch the tests:
```python 
python -m unittest discover 
```

If all tests are passed then run connection pool runner:
```python 
python src/multiple_connections_runner.py
```

You can also build postgres image:
```python 
docker-compose up -d
```
