import itertools
import threading


class Connection:
    """Database connection object."""

    connection_id = itertools.count(1)

    def __init__(self, connection_obj) -> None:
        self.connection: str = connection_obj
        self._is_free: float = True
        self.id: int = next(Connection.connection_id)

    def set_as_used(self) -> None:
        self._is_free = False

    def set_as_free(self) -> None:
        self._is_free = True

    def __str__(self) -> str:
        return f"#{self.id} connection"


class ConnectionManager:
    """Manager of connections."""

    def __init__(self, conn_data: str, min_amount=5, max_amount=30):
        self.conn_data = conn_data
        self.connections = [
            Connection(self.conn_data) for i in range(1, min_amount + 1)
        ]
        self.semaphore = threading.Semaphore()
        self.max_amount = max_amount

    def get_free_conn(self):
        self.semaphore.acquire()
        for conn in self.connections:
            if conn._is_free:
                return self._release_semaphore(conn)
        if not self._check_max_connection_amount():
            conn = self.add_new_connection()
            return self._release_semaphore(conn)
        self.semaphore.release()
        return None

    def _release_semaphore(self, conn):
        conn.set_as_used()
        self.semaphore.release()
        return conn

    def _drop_unused_connection(self):
        unused_connections = [
            connection
            for connection in self.connections
            if connection._is_free is False
        ]
        for conection in unused_connections:
            conection._is_free = True

    def amount_free_conn(self):
        result = sum(connection._is_free for connection in self.connections)
        return result

    def _check_max_connection_amount(self):
        if len(self.connections) >= self.max_amount:
            return True
        return False

    def add_new_connection(self):
        self.connections.append(Connection(self.conn_data))
        return self.connections[-1]
