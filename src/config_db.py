import psycopg2
import configparser

def config(filename="database.ini", section="postgresql"):
    parser = configparser.ConfigParser()
    parser.read(filename)

    db = {}

    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception(
            "Section {0} not found in the {1} file".format(section, filename)
        )

    return db


class DatabaseSQL:
    def __init__(self, params=config()):
        self.connection = psycopg2.connect(**params)

    def create_cursor(self):
        with self.connection.cursor() as self.cur:
            self.cur = self.connection.cursor()
            return self.cur

    def get_users(self):
        self.create_cursor()
        self.cur.execute("SELECT * FROM users;")
        users = self.cur.fetchall()
        return users


db = DatabaseSQL()


def get_database_connection():
    db_obj_conn = db.connection
    return db_obj_conn


print(db.get_users())
# print(get_database_connection())

# CREATE TABLE users(name VARCHAR(250) NOT NULL, surrname VARCHAR(250) NOT NULL);

# INSERT INTO users(name, surrname) VALUES ('pablo', 'roz2');
