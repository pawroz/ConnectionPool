"""
Test module for unittests.
"""

import unittest
from src.connectionmanager import ConnectionManager
from src.config_db import get_database_connection, db

# from src.multiple_connections_runner import db_queries

pool = ConnectionManager(get_database_connection())


class ConnPoolTest(unittest.TestCase):
    def test_set_up_database(self):
        self.assertIsNotNone(get_database_connection())

    def test_correct_db_query(self):
        self.assertIsNotNone(db.get_users())

    def test_min_and_max_amount_connection(self):
        self.assertIsNotNone(len(pool.connections))

    def test_added_new_connection(self):
        old_connections_amount: int = len(pool.connections)
        pool.add_new_connection()
        self.assertEqual(len(pool.connections), old_connections_amount + 1)

    # def test_added_new_connection(self):
    #     if len(db_queries) > len(pool.connections):
    #         amount_difference: int = len(db_queries) - len(pool.connections)
    #         pool.connections = [pool.add_new_connection() for _ in range(amount_difference)]
    #         self.assertEqual(pool.connections, len(db_queries))


if __name__ == "__main__":
    unittest.main()
