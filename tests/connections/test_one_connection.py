"""
Test module for test how long will it take to
execute query 500 times if you connect to the database and disconnect each time. 
"""

import time
import unittest

from src.config_db import DatabaseSQL

query = "SELECT * FROM users"


class TestConnection(unittest.TestCase):
    def test_connection(self) -> str:
        print("test started")
        start = time.perf_counter()
        for _ in range(500):
            self.select_query()
        finish = time.perf_counter()
        print(f"Finished in {round(finish-start, 2)} seconds(s)")

    def select_query(self) -> None:
        database_conn = DatabaseSQL().connection
        curr = database_conn.cursor()
        curr.execute(query)
        curr.fetchall()
        database_conn.close()


if __name__ == "__main__":
    unittest.main()


# def select_query():
#     database_conn = DatabaseSQL().connection
#     curr = database_conn.cursor()
#     curr.execute("SELECT * FROM users;")
#     users = curr.fetchall()
#     print(users)
#     print(curr)
#     database_conn.close()
#     print(database_conn)
#     curr.close()
#     #     db.connection.close()

# start = time.perf_counter()
# for _ in range(20):
#     select_query()
# finish = time.perf_counter()
# print(f"Finished in {round(finish-start, 2)} seconds(s)")
