"""
Test module for test how long will it take to
execute query 500 times if you connect ONCE to the database and receiving queries. 
"""

import time
import unittest
from concurrent.futures import ThreadPoolExecutor

from src.connectionmanager import ConnectionManager
from src.config_db import get_database_connection

query: str = "SELECT * FROM users"
pool = ConnectionManager(get_database_connection())
db_queries: list = [db_query for db_query in range(1, 500)]
start: float = time.perf_counter()


class TestMultiConnection(unittest.TestCase):
    def test_multiple_connection(db_query) -> str:
        conn = pool.get_free_conn()
        while not conn:
            print(
                f"For query {db_query} MAX amount connection reached. Retrying to reconnect."
            )
            conn = pool.get_free_conn()
            # time.sleep(2)
        print(
            f"{conn} and #{db_query} query now is busy. \t Amount of free connections = {pool.amount_free_conn()}"
        )
        with conn.connection.cursor() as cur:
            # print(pool.drop_unused_connection())
            cur.execute(query)
            result = cur.fetchall()
            # time.sleep(2)
            conn.set_as_free()
            print(f"Result of {conn} and #{db_query} query -  {result}")

    with ThreadPoolExecutor(len(db_queries)) as executor:
        fetches = executor.map(test_multiple_connection, db_queries)


finish = time.perf_counter()
print(f"Finished in {round(finish-start, 2)} seconds(s)")


if __name__ == "__main__":
    unittest.main()
